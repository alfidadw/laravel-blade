<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('register');
    }

    public function welcome(request $request){
        $fname = $request->firstname;
        $lname = $request->lastname;
        return view ('welcome', compact('fname', 'lname'));
    }
}
